﻿namespace Test_KitRum.Domain.Enums
{
    public enum AnimalType
    {
        Other,
        Cat,
        Dog,
        Parrot,
        Hamster,
        Fish,
        Rabbit
    }
}

﻿using Test_KitRum.Domain.Common;
using Test_KitRum.Domain.Enums;
using System;

namespace Test_KitRum.Domain.Entities
{
    public class Pet : AuditableEntity
    {
        public int Id { get; set; }

        public int EmployeeId { get; set; }

        public string Name { get; set; }

        public AnimalType AnimalType { get; set; }
    }
}

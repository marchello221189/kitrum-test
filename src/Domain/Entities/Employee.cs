﻿using Test_KitRum.Domain.Common;
using System.Collections.Generic;

namespace Test_KitRum.Domain.Entities
{
    public class Employee : AuditableEntity
    {
        public Employee()
        {
            Pets = new List<Pet>();
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public string LastName { get; set; }

        public bool IsMediaInteractive { get; set; }

        public IList<Pet> Pets { get; set; }
    }
}

﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using Microsoft.EntityFrameworkCore;
using Test_KitRum.Application.Common.Interfaces;

namespace Test_KitRum.Application.Employees.Commands.UpdateEmployee
{
    public class UpdateEmployeeCommandValidator : AbstractValidator<UpdateEmployeeCommand>
    {
        private readonly IApplicationDbContext _context;

        public UpdateEmployeeCommandValidator(IApplicationDbContext context)
        {
            _context = context;

            RuleFor(v => v.Name)
                .NotEmpty().WithMessage("Name is required.")
                .MaximumLength(50).WithMessage("Name must not exceed 50 characters.");

            RuleFor(v => v.LastName)
                .NotEmpty().WithMessage("Last Name is required.")
                .MaximumLength(50).WithMessage("Last Name must not exceed 50 characters.");

            RuleFor(v => v)
                .MustAsync(BeUniqueTitle).WithMessage("The specified Name and Last Name already exists.");
        }

        public async Task<bool> BeUniqueTitle(UpdateEmployeeCommand command, CancellationToken cancellationToken)
        {
            return await _context.Employees
                .Where(l => l.Id != command.Id)
                .AllAsync(l => 
                        l.Name != command.Name && 
                        l.LastName != command.LastName,
                    cancellationToken);
        }
    }
}

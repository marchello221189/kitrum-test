﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Test_KitRum.Application.Common.Exceptions;
using Test_KitRum.Application.Common.Interfaces;
using Test_KitRum.Domain.Entities;

namespace Test_KitRum.Application.Employees.Commands.UpdateEmployee
{
    public class UpdateEmployeeCommand : IRequest
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public bool IsMediaInteractive { get; set; }
    }

    public class UpdateTodoListCommandHandler : IRequestHandler<UpdateEmployeeCommand>
    {
        private readonly IApplicationDbContext _context;

        public UpdateTodoListCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Unit> Handle(UpdateEmployeeCommand request, CancellationToken cancellationToken)
        {
            var entity = await _context.Employees.FindAsync(request.Id);

            if (entity == null)
            {
                throw new NotFoundException(nameof(Employee), request.Id);
            }

            entity.Name = request.Name;
            entity.LastName = request.LastName;
            entity.IsMediaInteractive = request.IsMediaInteractive;

            await _context.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}

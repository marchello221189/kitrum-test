﻿using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using Microsoft.EntityFrameworkCore;
using Test_KitRum.Application.Common.Interfaces;

namespace Test_KitRum.Application.Employees.Commands.CreateEmployee
{
    public class CreateEmployeeCommandValidator : AbstractValidator<CreateEmployeeCommand>
    {
        private readonly IApplicationDbContext _context;

        public CreateEmployeeCommandValidator(IApplicationDbContext context)
        {
            _context = context;

            RuleFor(v => v.Name)
                .NotEmpty().WithMessage("Name is required.")
                .MaximumLength(50).WithMessage("Name must not exceed 50 characters.");

            RuleFor(v => v.LastName)
                .NotEmpty().WithMessage("Last Name is required.")
                .MaximumLength(50).WithMessage("Last Name must not exceed 50 characters.");

            RuleFor(v => v)
                .MustAsync(BeUniqueTitle).WithMessage("The specified employee already exists.");
        }

        public async Task<bool> BeUniqueTitle(CreateEmployeeCommand command, CancellationToken cancellationToken)
        {
            return await _context.Employees
                .AllAsync(employee => employee.Name != command.Name
                                      && employee.LastName != command.LastName,
                    cancellationToken);
        }
    }
}
﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Test_KitRum.Application.Common.Interfaces;
using Test_KitRum.Domain.Entities;

namespace Test_KitRum.Application.Employees.Commands.CreateEmployee
{
    public class CreateEmployeeCommand : IRequest<int>
    {
        public string Name { get; set; }
        public string LastName { get; set; }
        public bool  IsMediaInteractive { get; set; }
    }

    public class CreateEmployeeCommandHandler : IRequestHandler<CreateEmployeeCommand, int>
    {
        private readonly IApplicationDbContext _context;

        public CreateEmployeeCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<int> Handle(CreateEmployeeCommand request, CancellationToken cancellationToken)
        {
            var entity = new Employee
            {
                Name = request.Name, 
                LastName = request.LastName, 
                IsMediaInteractive = request.IsMediaInteractive
            };

            await _context.Employees.AddAsync(entity, cancellationToken);

            await _context.SaveChangesAsync(cancellationToken);

            return entity.Id;
        }
    }
}

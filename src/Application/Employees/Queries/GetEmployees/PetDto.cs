﻿using AutoMapper;
using Test_KitRum.Application.Common.Mappings;
using Test_KitRum.Domain.Entities;
using Test_KitRum.Domain.Enums;

namespace Test_KitRum.Application.Employees.Queries.GetEmployees
{
    public class PetDto : IMapFrom<Pet>
    {
        public int Id { get; set; }

        public int EmployeeId { get; set; }

        public string Name { get; set; }

        public AnimalType AnimalType { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<Pet, PetDto>()
                .ForMember(d => d.AnimalType, opt => opt.MapFrom(s => s.AnimalType));
        }
    }
}

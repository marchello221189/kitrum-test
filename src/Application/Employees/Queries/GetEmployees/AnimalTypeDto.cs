﻿namespace Test_KitRum.Application.Employees.Queries.GetEmployees
{
    public class AnimalTypeDto
    {
        public int Value { get; set; }

        public string Name { get; set; }
    }
}

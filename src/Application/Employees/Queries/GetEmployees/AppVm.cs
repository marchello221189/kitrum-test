﻿using System.Collections.Generic;

namespace Test_KitRum.Application.Employees.Queries.GetEmployees
{
    public class AppVm
    {
        public IList<AnimalTypeDto> AnimalTypes { get; set; }

        public IList<EmployeeDto> Employees { get; set; }
    }
}

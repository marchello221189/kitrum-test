﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Test_KitRum.Application.Common.Interfaces;
using Test_KitRum.Domain.Enums;

namespace Test_KitRum.Application.Employees.Queries.GetEmployees
{
    public class GetEmployeeQuery : IRequest<AppVm>
    {
    }

    public class GetEmployeeQueryHandler : IRequestHandler<GetEmployeeQuery, AppVm>
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetEmployeeQueryHandler(IApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<AppVm> Handle(GetEmployeeQuery request, CancellationToken cancellationToken)
        {
            return new AppVm
            {
                AnimalTypes = Enum.GetValues(typeof(AnimalType))
                    .Cast<AnimalType>()
                    .Select(p => new AnimalTypeDto { Value = (int)p, Name = p.ToString() })
                    .ToList(),

                Employees = await _context.Employees
                    .ProjectTo<EmployeeDto>(_mapper.ConfigurationProvider)
                    .OrderBy(t => t.Name)
                    .ToListAsync(cancellationToken)
            };
        }
    }
}

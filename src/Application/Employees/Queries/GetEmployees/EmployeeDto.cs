﻿using System.Collections.Generic;
using Test_KitRum.Application.Common.Mappings;
using Test_KitRum.Domain.Entities;

namespace Test_KitRum.Application.Employees.Queries.GetEmployees
{
    public class EmployeeDto : IMapFrom<Employee>
    {
        public EmployeeDto()
        {
            Pets = new List<PetDto>();
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public string LastName { get; set; }

        public bool IsMediaInteractive { get; set; }

        public IList<PetDto> Pets { get; set; }
    }
}
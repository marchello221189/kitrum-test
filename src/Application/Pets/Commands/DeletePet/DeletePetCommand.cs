﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Test_KitRum.Application.Common.Exceptions;
using Test_KitRum.Application.Common.Interfaces;
using Test_KitRum.Domain.Entities;

namespace Test_KitRum.Application.Pets.Commands.DeletePet
{
    public class DeletePetCommand : IRequest
    {
        public int Id { get; set; }
    }

    public class DeleteTodoItemCommandHandler : IRequestHandler<DeletePetCommand>
    {
        private readonly IApplicationDbContext _context;

        public DeleteTodoItemCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Unit> Handle(DeletePetCommand request, CancellationToken cancellationToken)
        {
            var entity = await _context.Pets.FindAsync(request.Id);

            if (entity == null)
            {
                throw new NotFoundException(nameof(Pet), request.Id);
            }

            _context.Pets.Remove(entity);

            await _context.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}

﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Test_KitRum.Application.Common.Interfaces;
using Test_KitRum.Domain.Entities;
using Test_KitRum.Domain.Enums;

namespace Test_KitRum.Application.Pets.Commands.CreatePet
{
    public class CreatePetCommand : IRequest<int>
    {
        public int EmployeeId { get; set; }

        public string Name { get; set; }

        public AnimalType AnimalType { get; set; }
    }

    public class CreatePetCommandHandler : IRequestHandler<CreatePetCommand, int>
    {
        private readonly IApplicationDbContext _context;

        public CreatePetCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<int> Handle(CreatePetCommand request, CancellationToken cancellationToken)
        {
            var entity = new Pet
            {
                EmployeeId = request.EmployeeId,
                Name = request.Name,
                AnimalType = request.AnimalType
            };

            await _context.Pets.AddAsync(entity, cancellationToken);

            await _context.SaveChangesAsync(cancellationToken);

            return entity.Id;
        }
    }
}

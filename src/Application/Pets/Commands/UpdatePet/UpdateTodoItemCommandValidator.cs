﻿using FluentValidation;

namespace Test_KitRum.Application.Pets.Commands.UpdatePet
{
    public class UpdateTodoItemCommandValidator : AbstractValidator<UpdatePetCommand>
    {
        public UpdateTodoItemCommandValidator()
        {
            RuleFor(v => v.Name)
                .NotEmpty().WithMessage("Name is required.")
                .MaximumLength(50).WithMessage("Name must not exceed 50 characters.");
        }
    }
}

﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Test_KitRum.Application.Common.Exceptions;
using Test_KitRum.Application.Common.Interfaces;
using Test_KitRum.Domain.Entities;
using Test_KitRum.Domain.Enums;

namespace Test_KitRum.Application.Pets.Commands.UpdatePet
{
    public class UpdatePetCommand : IRequest
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public AnimalType AnimalType { get; set; }
    }

    public class UpdateTodoItemCommandHandler : IRequestHandler<UpdatePetCommand>
    {
        private readonly IApplicationDbContext _context;

        public UpdateTodoItemCommandHandler(IApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Unit> Handle(UpdatePetCommand request, CancellationToken cancellationToken)
        {
            var entity = await _context.Pets.FindAsync(request.Id);

            if (entity == null)
            {
                throw new NotFoundException(nameof(Pet), request.Id);
            }

            entity.Name = request.Name;
            entity.AnimalType = request.AnimalType;

            await _context.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
﻿using Test_KitRum.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using System.Threading.Tasks;

namespace Test_KitRum.Application.Common.Interfaces
{
    public interface IApplicationDbContext
    {
        DbSet<Employee> Employees { get; set; }

        DbSet<Pet> Pets { get; set; }

        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
    }
}

﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Test_KitRum.Application.Pets.Commands.CreatePet;
using Test_KitRum.Application.Pets.Commands.DeletePet;
using Test_KitRum.Application.Pets.Commands.UpdatePet;

namespace Test_KitRum.WebUI.Controllers
{
    [Authorize]
    public class PetsController : ApiController
    {
        [HttpPost]
        public async Task<ActionResult<int>> Create(CreatePetCommand command)
        {
            return await Mediator.Send(command);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult> Update(int id, [FromBody]UpdatePetCommand command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }

            await Mediator.Send(command);

            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            await Mediator.Send(new DeletePetCommand { Id = id });

            return NoContent();
        }
    }
}

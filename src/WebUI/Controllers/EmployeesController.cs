﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Test_KitRum.Application.Employees.Commands.CreateEmployee;
using Test_KitRum.Application.Employees.Commands.DeleteEmployee;
using Test_KitRum.Application.Employees.Commands.UpdateEmployee;
using Test_KitRum.Application.Employees.Queries.GetEmployees;

namespace Test_KitRum.WebUI.Controllers
{
    [Authorize]
    public class EmployeesController : ApiController
    {
        [HttpGet]
        public async Task<ActionResult<AppVm>> Get()
        {
            return await Mediator.Send(new GetEmployeeQuery());
        }

        [HttpPost]
        public async Task<ActionResult<int>> Create([FromBody] CreateEmployeeCommand command)
        {
            return await Mediator.Send(command);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult> Update(int id, [FromBody] UpdateEmployeeCommand command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }

            await Mediator.Send(command);

            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            await Mediator.Send(new DeleteEmployeeCommand { Id = id });

            return NoContent();
        }
    }
}

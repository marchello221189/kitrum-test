import { Component, TemplateRef } from '@angular/core';
import { EmployeesClient, CreatePetCommand, PetDto, UpdatePetCommand,
  AppVm, PetsClient, EmployeeDto, CreateEmployeeCommand, UpdateEmployeeCommand, AnimalType} from '../test_kitrum-api';
import { faPlus, faEllipsisH } from '@fortawesome/free-solid-svg-icons';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

@Component({
    selector: 'app-todo-component',
    templateUrl: './employee.component.html',
    styleUrls: ['./employee.component.css']
})
export class EmployeeComponent {

    debug = false;

    vm: AppVm;

    selectedEmployee: EmployeeDto;
    selectedPet: PetDto;

    newEmployeeEditor: any = {};
    employeeUpdateEditor: any = {};
    petDetailsEditor: any = {};

    newEmployeeModalRef: BsModalRef;
    employeeOptionsModalRef: BsModalRef;
    deleteEmployeeModalRef: BsModalRef;
    petDetailsModalRef: BsModalRef;

    faPlus = faPlus;
    faEllipsisH = faEllipsisH;
    AnimalTypes = AnimalType;

    constructor(private employeeClient: EmployeesClient, private petsClient: PetsClient, private modalService: BsModalService) {
        employeeClient.get().subscribe(
            result => {
                this.vm = result;
                if (this.vm.employees.length) {
                    this.selectedEmployee = this.vm.employees[0];
                }
            },
            error => console.error(error)
        );
    }

    // Lists
    petsAmount(list: EmployeeDto): number {
        return list?.pets?.length ?? 0;
    }

    showNewEmployeeModal(template: TemplateRef<any>): void {
        this.newEmployeeModalRef = this.modalService.show(template);
        setTimeout(() => document.getElementById('employeeName').focus(), 250);
    }

    newEmployeeCancelled(): void {
        this.newEmployeeModalRef.hide();
        this.newEmployeeEditor = {};
    }

    addEmployee(): void {
        const employee = EmployeeDto.fromJS({
            id: 0,
            name: this.newEmployeeEditor.name,
            lastName: this.newEmployeeEditor.lastName,
            isMediaInteractive: this.newEmployeeEditor.isMediaInteractive,
            items: [],
        });
        this.employeeClient.create(<CreateEmployeeCommand>{
          name: this.newEmployeeEditor.name,
          lastName: this.newEmployeeEditor.lastName})
          .subscribe(
            result => {
                employee.id = result;
                this.vm.employees.push(employee);
                this.selectedEmployee = employee;
                this.newEmployeeModalRef.hide();
                this.newEmployeeEditor = {};
            },
            error => {
                const errors = JSON.parse(error.response);

                if (errors && errors.Title) {
                    this.newEmployeeEditor.error = errors.Title[0];
                }

                setTimeout(() => document.getElementById('title').focus(), 250);
            }
        );
    }

    showEmployeeUpdateModal(template: TemplateRef<any>) {
        this.employeeUpdateEditor = {
            id: this.selectedEmployee.id,
            name: this.selectedEmployee.name,
            lastName: this.selectedEmployee.lastName,
        };

        this.employeeOptionsModalRef = this.modalService.show(template);
    }

    updateEmployeeOptions() {
        this.employeeClient.update(this.selectedEmployee.id, UpdateEmployeeCommand.fromJS(this.employeeUpdateEditor))
            .subscribe(
                () => {
                    this.selectedEmployee.name = this.employeeUpdateEditor.name,
                    this.selectedEmployee.lastName = this.employeeUpdateEditor.lastName,
                    this.selectedEmployee.isMediaInteractive = this.employeeUpdateEditor.isMediaInteractive,
                    this.employeeOptionsModalRef.hide();
                    this.employeeUpdateEditor = {};
                },
                error => console.error(error)
            );
    }

    confirmDeleteEmployee(template: TemplateRef<any>) {
        this.employeeOptionsModalRef.hide();
        this.deleteEmployeeModalRef = this.modalService.show(template);
    }

    deleteEmployeeConfirmed(): void {
        this.employeeClient.delete(this.selectedEmployee.id).subscribe(
            () => {
                this.deleteEmployeeModalRef.hide();
                this.vm.employees = this.vm.employees.filter(t => t.id !== this.selectedEmployee.id);
                this.selectedEmployee = this.vm.employees.length ? this.vm.employees[0] : null;
            },
            error => console.error(error)
        );
    }

    // Pets

    showPetDetailsModal(template: TemplateRef<any>, item: PetDto): void {
        this.selectedPet = item;
        this.petDetailsEditor = {
            ...this.selectedPet
        };

        this.petDetailsModalRef = this.modalService.show(template);
    }

    updatePetDetails(): void {
        this.petsClient.update(this.selectedPet.id, UpdatePetCommand.fromJS(this.petDetailsEditor))
            .subscribe(
                () => {
                    if (this.selectedPet.employeeId !== this.petDetailsEditor.employeeId) {
                        this.selectedEmployee.pets = this.selectedEmployee.pets.filter(i => i.id !== this.selectedPet.id);
                        const employeeIndex = this.vm.employees.findIndex(l => l.id === this.petDetailsEditor.employeeId);
                        this.selectedPet.employeeId = this.petDetailsEditor.listId;
                        this.vm.employees[employeeIndex].pets.push(this.selectedPet);
                    }

                    this.selectedPet.animalType = this.petDetailsEditor.animalType;
                    this.selectedPet.name = this.petDetailsEditor.name;
                    this.petDetailsModalRef.hide();
                    this.petDetailsEditor = {};
                    this.selectedPet = null;
                },
                error => console.error(error)
            );
    }

    addPet() {
        const pet = PetDto.fromJS({
            id: 0,
            employeeId: this.selectedEmployee.id,
            animalType: 0,
            name: ''
        });

        this.selectedEmployee.pets.push(pet);
        const index = this.selectedEmployee.pets.length - 1;
        this.editPet(pet, 'name' + index);
    }

    editPet(item: PetDto, inputId: string): void {
        this.selectedPet = item;
        setTimeout(() => document.getElementById(inputId).focus(), 100);
    }

    updatePet(pet: PetDto, pressedEnter: boolean = false): void {
        const isNewPet = pet.id === 0;

        if (!pet.name.trim()) {
            this.deletePet(pet);
            return;
        }

        if (isNewPet) {
            this.petsClient.create(CreatePetCommand.fromJS({ ...pet, employeeId: this.selectedEmployee.id }))
                .subscribe(
                    result => {
                        pet.id = result;
                    },
                    error => console.error(error)
                );
        } else {
            this.petsClient.update(pet.id, UpdatePetCommand.fromJS(pet))
                .subscribe(
                    () => console.log('Update succeeded.'),
                    error => console.error(error)
                );
        }

        this.selectedPet = null;

        if (isNewPet && pressedEnter) {
            this.addPet();
        }
    }

    // Delete pet
    deletePet(petDto: PetDto) {
        if (this.petDetailsModalRef) {
            this.petDetailsModalRef.hide();
        }

        if (petDto.id === 0) {
            const petIndex = this.selectedEmployee.pets.indexOf(this.selectedPet);
            this.selectedEmployee.pets.splice(petIndex, 1);
        } else {
            this.petsClient.delete(petDto.id).subscribe(
                () => this.selectedEmployee.pets = this.selectedEmployee.pets.filter(t => t.id !== petDto.id),
                error => console.error(error)
            );
        }
    }

  close(petDetailsModalRef: BsModalRef) {
    petDetailsModalRef.hide();
    this.selectedPet = null;
  }
}

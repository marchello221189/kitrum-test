﻿using Test_KitRum.Application.Common.Interfaces;
using System;

namespace Test_KitRum.Infrastructure.Services
{
    public class DateTimeService : IDateTime
    {
        public DateTime Now => DateTime.Now;
    }
}

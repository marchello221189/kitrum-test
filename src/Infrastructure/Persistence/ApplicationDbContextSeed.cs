﻿using Test_KitRum.Domain.Entities;
using Test_KitRum.Infrastructure.Identity;
using Microsoft.AspNetCore.Identity;
using System.Linq;
using System.Threading.Tasks;
using Test_KitRum.Domain.Enums;

namespace Test_KitRum.Infrastructure.Persistence
{
    public static class ApplicationDbContextSeed
    {
        public static async Task SeedDefaultUserAsync(UserManager<ApplicationUser> userManager)
        {
            var defaultUser = new ApplicationUser { UserName = "administrator@localhost", Email = "administrator@localhost" };

            if (userManager.Users.All(u => u.UserName != defaultUser.UserName))
            {
                await userManager.CreateAsync(defaultUser, "Administrator1!");
            }
        }

        public static async Task SeedSampleDataAsync(ApplicationDbContext context)
        {
            // Seed, if necessary
            if (!context.Employees.Any())
            {
                await context.Employees.AddAsync(new Employee
                {
                    Name = "Isabella",
                    LastName = "Mason",
                    Pets =
                    {
                        new Pet { Name = "Molly", AnimalType = AnimalType.Cat},
                        new Pet { Name = "Bella", AnimalType = AnimalType.Parrot },
                        new Pet { Name = "Rex", AnimalType = AnimalType.Dog}
                    }
                });

                await context.Employees.AddAsync(new Employee
                {
                    Name = "Emma",
                    LastName = "William",
                    Pets =
                    {
                        new Pet { Name = "Coco", AnimalType = AnimalType.Cat},
                        new Pet { Name = "Tuna", AnimalType = AnimalType.Fish },
                        new Pet { Name = "Charlie", AnimalType = AnimalType.Dog}
                    }
                });

                await context.Employees.AddAsync(new Employee
                {
                    Name = "Olivia",
                    LastName = "Jayden",
                    Pets =
                    {
                        new Pet { Name = "Lucy", AnimalType = AnimalType.Cat},
                        new Pet { Name = "Oscar", AnimalType = AnimalType.Fish },
                        new Pet { Name = "Max", AnimalType = AnimalType.Dog}
                    }
                });

                await context.SaveChangesAsync();
            }
        }
    }
}

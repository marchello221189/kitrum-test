﻿using Microsoft.AspNetCore.Identity;

namespace Test_KitRum.Infrastructure.Identity
{
    public class ApplicationUser : IdentityUser
    {
    }
}

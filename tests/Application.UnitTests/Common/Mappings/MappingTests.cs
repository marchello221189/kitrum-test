﻿using AutoMapper;
using Test_KitRum.Application.Common.Mappings;
using Test_KitRum.Domain.Entities;
using NUnit.Framework;
using System;
using Test_KitRum.Application.Employees.Queries.GetEmployees;

namespace Test_KitRum.Application.UnitTests.Common.Mappings
{
    public class MappingTests
    {
        private readonly IConfigurationProvider _configuration;
        private readonly IMapper _mapper;

        public MappingTests()
        {
            _configuration = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<MappingProfile>();
            });

            _mapper = _configuration.CreateMapper();
        }

        [Test]
        public void ShouldHaveValidConfiguration()
        {
            _configuration.AssertConfigurationIsValid();
        }
        
        [Test]
        [TestCase(typeof(Employee), typeof(EmployeeDto))]
        [TestCase(typeof(Pet), typeof(PetDto))]
        public void ShouldSupportMappingFromSourceToDestination(Type source, Type destination)
        {
            var instance = Activator.CreateInstance(source);

            _mapper.Map(instance, source, destination);
        }
    }
}

﻿using System.Threading.Tasks;
using FluentAssertions;
using NUnit.Framework;
using Test_KitRum.Application.Common.Exceptions;
using Test_KitRum.Application.Employees.Commands.CreateEmployee;
using Test_KitRum.Application.Employees.Commands.DeleteEmployee;
using Test_KitRum.Domain.Entities;

namespace Test_KitRum.Application.IntegrationTests.Employees.Commands
{
    using static Testing;

    public class DeleteEmployeeTests : TestBase
    {
        [Test]
        public void ShouldRequireValidEmployeeId()
        {
            var command = new DeleteEmployeeCommand { Id = 99 };

            FluentActions.Invoking(() =>
                SendAsync(command)).Should().Throw<NotFoundException>();
        }

        [Test]
        public async Task ShouldDeleteEmployee()
        {
            var employeeId =   await SendAsync(new CreateEmployeeCommand
            {
                Name = "TestName",
                LastName = "TestLastName"
            });

            await SendAsync(new DeleteEmployeeCommand 
            { 
                Id = employeeId 
            });

            var employee = await FindAsync<Employee>(employeeId);

            employee.Should().BeNull();
        }
    }
}

﻿using System;
using System.Threading.Tasks;
using FluentAssertions;
using NUnit.Framework;
using Test_KitRum.Application.Common.Exceptions;
using Test_KitRum.Application.Employees.Commands.CreateEmployee;
using Test_KitRum.Domain.Entities;

namespace Test_KitRum.Application.IntegrationTests.Employees.Commands
{
    using static Testing;

    public class CreateEmployeeTests : TestBase
    {
        [Test]
        public void ShouldRequireMinimumFields()
        {
            var command = new CreateEmployeeCommand();

            FluentActions.Invoking(() =>
                SendAsync(command)).Should().Throw<ValidationException>();
        }

        [Test]
        public async Task ShouldRequireUniqueNameAndLastName()
        {
            await SendAsync(new CreateEmployeeCommand
            {
                Name = "TestName",
                LastName = "TestLastName"
            });

            var command = new CreateEmployeeCommand
            {
                Name = "TestName",
                LastName = "TestLastName"
            };

            FluentActions.Invoking(() =>
                SendAsync(command)).Should().Throw<ValidationException>();
        }

        [Test]
        public async Task ShouldCreateEmployeeList()
        {
            var userId = await RunAsDefaultUserAsync();

            var command = new CreateEmployeeCommand
            {
                Name = "TestName",
                LastName = "TestLastName",
                IsMediaInteractive = true
            };

            var id = await SendAsync(command);

            var employee = await FindAsync<Employee>(id);

            employee.Should().NotBeNull();
            employee.Name.Should().Be(command.Name);
            employee.LastName.Should().Be(command.LastName);
            employee.IsMediaInteractive.Should().Be(command.IsMediaInteractive);
            employee.CreatedBy.Should().Be(userId);
            employee.Created.Should().BeCloseTo(DateTime.Now, 10000);
        }
    }
}

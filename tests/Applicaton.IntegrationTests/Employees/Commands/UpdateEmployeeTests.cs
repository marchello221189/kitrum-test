﻿using System;
using System.Threading.Tasks;
using FluentAssertions;
using NUnit.Framework;
using Test_KitRum.Application.Common.Exceptions;
using Test_KitRum.Application.Employees.Commands.CreateEmployee;
using Test_KitRum.Application.Employees.Commands.UpdateEmployee;
using Test_KitRum.Domain.Entities;

namespace Test_KitRum.Application.IntegrationTests.Employees.Commands
{
    using static Testing;

    public class UpdateEmployeeTests : TestBase
    {
        [Test]
        public void ShouldRequireValidEmployeeId()
        {
            var command = new UpdateEmployeeCommand
            {
                Id = 99,
                Name = "TestName",
                LastName = "TestLastName"
            };

            FluentActions.Invoking(() =>
                SendAsync(command)).Should().Throw<NotFoundException>();
        }

        [Test]
        public async Task ShouldRequireUniqueTitle()
        {
            var employeeId = await SendAsync(new CreateEmployeeCommand
            {
                Name = "TestName",
                LastName = "TestLastName"
            });

            await SendAsync(new CreateEmployeeCommand
            {
                Name = "TestName2",
                LastName = "TestLastName2"
            });

            var command = new UpdateEmployeeCommand
            {
                Id = employeeId,
                Name = "TestName2",
                LastName = "TestLastName2"
            };

            FluentActions.Invoking(() =>
                    SendAsync(command))
                .Should().Throw<ValidationException>();
        }

        [Test]
        public async Task ShouldUpdateEmployee()
        {
            var userId = await RunAsDefaultUserAsync();

            var employeeId = await SendAsync(new CreateEmployeeCommand
            {
                Name = "TestName",
                LastName = "TestLastName"
            });

            var command = new UpdateEmployeeCommand
            {
                Id = employeeId,
                Name = "TestName2",
                LastName = "TestLastName2",
                IsMediaInteractive = true
            };

            await SendAsync(command);

            var employee = await FindAsync<Employee>(employeeId);

            employee.Name.Should().Be(command.Name);
            employee.LastName.Should().Be(command.LastName);
            employee.IsMediaInteractive.Should().Be(command.IsMediaInteractive);
            employee.LastModifiedBy.Should().NotBeNull();
            employee.LastModifiedBy.Should().Be(userId);
            employee.LastModified.Should().NotBeNull();
            employee.LastModified.Should().BeCloseTo(DateTime.Now, 1000);
        }
    }
}

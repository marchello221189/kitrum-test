﻿using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using NUnit.Framework;
using Test_KitRum.Application.Employees.Queries.GetEmployees;
using Test_KitRum.Domain.Entities;
using Test_KitRum.Domain.Enums;

namespace Test_KitRum.Application.IntegrationTests.Employees.Queries
{
    using static Testing;

    public class GetEmployeeVmTests : TestBase
    {
        [Test]
        public async Task ShouldReturnAnimalTypes()
        {
            var query = new GetEmployeeQuery();

            var result = await SendAsync(query);

            result.AnimalTypes.Should().NotBeEmpty();
        }

        [Test]
        public async Task ShouldReturnEmployeesAndPets()
        {
            await AddAsync(new Employee
            {
                Name = "TestName",
                LastName = "TestLastName",
                Pets =
                {
                    new Pet {Name = "TestName1", AnimalType = AnimalType.Dog},
                    new Pet {Name = "TestName2", AnimalType = AnimalType.Dog},
                    new Pet {Name = "TestName3", AnimalType = AnimalType.Dog},
                    new Pet {Name = "TestName4", AnimalType = AnimalType.Dog},
                    new Pet {Name = "TestName5", AnimalType = AnimalType.Dog},
                    new Pet {Name = "TestName6", AnimalType = AnimalType.Dog},
                    new Pet {Name = "TestName7", AnimalType = AnimalType.Dog}
                }
            });

            var query = new GetEmployeeQuery();

            var result = await SendAsync(query);

            result.Employees.Should().HaveCount(1);
            result.Employees.First().Pets.Should().HaveCount(7);
        }
    }
}
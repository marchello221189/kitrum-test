﻿using System;
using System.Threading.Tasks;
using FluentAssertions;
using NUnit.Framework;
using Test_KitRum.Application.Common.Exceptions;
using Test_KitRum.Application.Employees.Commands.CreateEmployee;
using Test_KitRum.Application.Pets.Commands.CreatePet;
using Test_KitRum.Application.Pets.Commands.UpdatePet;
using Test_KitRum.Domain.Entities;
using Test_KitRum.Domain.Enums;

namespace Test_KitRum.Application.IntegrationTests.Pets.Commands
{
    using static Testing;

    public class UpdatePetTests : TestBase
    {
        [Test]
        public void ShouldRequireValidPetId()
        {
            var command = new UpdatePetCommand
            {
                Id = 99,
                Name = "Rex",
                AnimalType = AnimalType.Dog
            };

            FluentActions.Invoking(() =>
                SendAsync(command)).Should().Throw<NotFoundException>();
        }

        [Test]
        public async Task ShouldUpdatePet()
        {
            var userId = await RunAsDefaultUserAsync();

            var employeeId = await SendAsync(new CreateEmployeeCommand
            {
                Name = "TestName",
                LastName = "TestLastName"
            });

            var petId = await SendAsync(new CreatePetCommand
            {
                EmployeeId = employeeId,
                Name = "Rex"
            });

            var command = new UpdatePetCommand
            {
                Id = petId,
                Name = "Rex-Cat",
                AnimalType = AnimalType.Cat
            };

            await SendAsync(command);

            var item = await FindAsync<Pet>(petId);

            item.EmployeeId.Should().Be(employeeId);
            item.Name.Should().Be(command.Name);
            item.AnimalType.Should().Be(command.AnimalType);
            item.LastModifiedBy.Should().NotBeNull();
            item.LastModifiedBy.Should().Be(userId);
            item.LastModified.Should().NotBeNull();
            item.LastModified.Should().BeCloseTo(DateTime.Now, 10000);
        }
    }
}

﻿using System;
using System.Threading.Tasks;
using FluentAssertions;
using NUnit.Framework;
using Test_KitRum.Application.Common.Exceptions;
using Test_KitRum.Application.Employees.Commands.CreateEmployee;
using Test_KitRum.Application.Pets.Commands.CreatePet;
using Test_KitRum.Domain.Entities;
using Test_KitRum.Domain.Enums;

namespace Test_KitRum.Application.IntegrationTests.Pets.Commands
{
    using static Testing;

    public class CreatePetTests : TestBase
    {
        [Test]
        public void ShouldRequireMinimumFields()
        {
            var command = new CreatePetCommand();

            FluentActions.Invoking(() =>
                SendAsync(command)).Should().Throw<ValidationException>();
        }

        [Test]
        public async Task ShouldCreatePet()
        {
            var userId = await RunAsDefaultUserAsync();

            var employeeId = await SendAsync(new CreateEmployeeCommand
            {
                Name = "TestName",
                LastName = "TestLastName"
            });

            var command = new CreatePetCommand
            {
                EmployeeId = employeeId,
                Name = "Rex",
                AnimalType = AnimalType.Dog
            };

            var itemId = await SendAsync(command);

            var item = await FindAsync<Pet>(itemId);

            item.Should().NotBeNull();
            item.EmployeeId.Should().Be(command.EmployeeId);
            item.Name.Should().Be(command.Name);
            item.AnimalType.Should().Be(command.AnimalType);
            item.CreatedBy.Should().Be(userId);
            item.Created.Should().BeCloseTo(DateTime.Now, 10000);
            item.LastModifiedBy.Should().BeNull();
            item.LastModified.Should().BeNull();
        }
    }
}

﻿using System.Threading.Tasks;
using FluentAssertions;
using NUnit.Framework;
using Test_KitRum.Application.Common.Exceptions;
using Test_KitRum.Application.Employees.Commands.CreateEmployee;
using Test_KitRum.Application.Pets.Commands.CreatePet;
using Test_KitRum.Application.Pets.Commands.DeletePet;
using Test_KitRum.Domain.Entities;

namespace Test_KitRum.Application.IntegrationTests.Pets.Commands
{
    using static Testing;

    public class DeletePetTests : TestBase
    {
        [Test]
        public void ShouldRequireValidPetId()
        {
            var command = new DeletePetCommand { Id = 99 };

            FluentActions.Invoking(() =>
                SendAsync(command)).Should().Throw<NotFoundException>();
        }

        [Test]
        public async Task ShouldDeletePet()
        {
            var employeeId = await SendAsync(new CreateEmployeeCommand
            {
                Name = "TestName",
                LastName = "TestLastName"
            });

            var petId = await SendAsync(new CreatePetCommand
            {
                EmployeeId = employeeId,
                Name = "Rex"
            });

            await SendAsync(new DeletePetCommand
            {
                Id = petId
            });

            var pet = await FindAsync<Pet>(petId);

            pet.Should().BeNull();
        }
    }
}
